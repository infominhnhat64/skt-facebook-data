import requests, urllib.parse, base64, json, time, re, random
import connectMongoDB


class GroupsProfile():

    def __init__(self):
        self.url = 'https://www.facebook.com/api/graphql/'
        self.headers = {
            'authority': 'www.facebook.com',
            'accept': '*/*',
            'accept-language': 'en-US,en;q=0.9,vi;q=0.8',
            'content-type': 'application/x-www-form-urlencoded',
            'cookie': '',
            'dpr': '1.125',
            'origin': 'https://www.facebook.com',
            'referer': 'https://www.facebook.com/',
            'sec-ch-prefers-color-scheme': 'dark',
            'sec-ch-ua': '"Chromium";v="122", "Not(A:Brand";v="24", "Microsoft Edge";v="122"',
            'sec-ch-ua-full-version-list': '"Chromium";v="122.0.6261.129", "Not(A:Brand";v="24.0.0.0", "Microsoft Edge";v="122.0.2365.92"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-model': '""',
            'sec-ch-ua-platform': '"Windows"',
            'sec-ch-ua-platform-version': '"15.0.0"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Edg/122.0.0.0',
            'viewport-width': '980',
            'x-asbd-id': '129477',
            'x-fb-friendly-name': 'ProfileCometAppCollectionListRendererPaginationQuery',
            'x-fb-lsd': 'Mz-y91vxBpwctoC6A7n5Gt'
        }

    def query_group(self, uid_profile):
        node_groups = []
        end_cursor = {'cursor': ''}

        def post_request():
            encoded_bytes = base64.b64encode(str("app_collection:" + uid_profile + ":2361831622:66").encode("utf-8"))
            variables = '{"count":8,"cursor":"' + end_cursor['cursor'] + '","scale":1.5,"search":null,"id":"' + str(
                encoded_bytes.decode("utf-8")) + '","__relay_internal__pv__VideoPlayerRelayReplaceDashManifestWithPlaylistrelayprovider":false}'
            payload = '&variables=' + urllib.parse.quote_plus(variables) + '&doc_id=8004529446241809k-route-definitions%2F'

            data_response = requests.request("POST", self.url, headers=self.headers, data=payload)
            return data_response

        def regex_groups(data_response):
            dataJson = json.loads(data_response.text)

            edges = dataJson['data']['node']['pageItems']['edges']

            for item in edges:
                node_gr = {

                    'groups_id': item['node']['node']['id'],
                    'groups_privacy': item['node']['node']['privacy_info']['title']['text'],
                    'groups_name': item['node']['title']['text'],
                    'groups_description': str(item['node']['subtitle_text']['text']).split("members")[1].replace("\n",""),
                    'groups_membership': str(item['node']['subtitle_text']['text']).split(" members")[0].replace(",",""),
                    'groups_url': item['node']['url'],
                }

                node_groups.append(node_gr)

        def page_info(data_response):

            dataJson = json.loads(data_response.text)

            has_next_page = dataJson['data']['node']['pageItems']['page_info']['has_next_page']
            cursor = dataJson['data']['node']['pageItems']['page_info']['end_cursor']
            if has_next_page != True:
                return True
            else:
                end_cursor.update({'cursor': cursor})
                print(has_next_page, end_cursor)

        while True:
            try:
                data_response = post_request()
                regex_groups(data_response)
                if page_info(data_response):
                    break
                time.sleep(random.randint(1,5))
            except:
                print(f'⚠️ Error: {data_response.text}')
        return node_groups

    def start_program(self):
        classname = 'GroupsProfile'
        arraydb = ['100045311072713', '100010594382155', '100009476251070', '100029503205429', '100045021730506', '100017134415554', '100033155542858', '100010293972896', '100005155240948', '100002625085966', '100021851658543', '100026134724674', '100034830286523', '100091789002429', '100081555198049', '100014359458991', '100037872704361', '100010688473637', '100073488045061']

        for uid_profile in arraydb:
            print(f'✅ Bắt đầu với UID: {uid_profile}')
            node_groups = self.query_group(uid_profile)
            groupsDB = {'person_uid': uid_profile,
                        'data': node_groups}

            connectMongoDB.insert(groupsDB, classname)
            print(groupsDB)

if __name__ == '__main__':
	main_app = GroupsProfile()
	main_app.start_program()
