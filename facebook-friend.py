import requests, urllib.parse, base64, json, time, re, random
import connectMongoDB


class FriendsProfile():

    def __init__(self):
        self.url = 'https://www.facebook.com/api/graphql/'
        self.headers = {
            'authority': 'www.facebook.com',
            'accept': '*/*',
            'accept-language': 'en-US,en;q=0.9,vi;q=0.8',
            'content-type': 'application/x-www-form-urlencoded',
            'cookie': '',
            'dpr': '1.125',
            'origin': 'https://www.facebook.com',
            'referer': 'https://www.facebook.com/',
            'sec-ch-prefers-color-scheme': 'dark',
            'sec-ch-ua': '"Chromium";v="122", "Not(A:Brand";v="24", "Microsoft Edge";v="122"',
            'sec-ch-ua-full-version-list': '"Chromium";v="122.0.6261.129", "Not(A:Brand";v="24.0.0.0", "Microsoft Edge";v="122.0.2365.92"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-model': '""',
            'sec-ch-ua-platform': '"Windows"',
            'sec-ch-ua-platform-version': '"15.0.0"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Edg/122.0.0.0',
            'viewport-width': '980',
            'x-asbd-id': '129477',
            'x-fb-friendly-name': 'ProfileCometAppCollectionListRendererPaginationQuery',
            'x-fb-lsd': 'Mz-y91vxBpwctoC6A7n5Gt'
        }

    def query_friend(self, uid_profile):
        node_friend = []
        end_cursor = {'cursor': ''}

        def post_request():

            encoded_bytes = base64.b64encode(str("app_collection:" + uid_profile + ":2356318349:2").encode("utf-8"))
            variables = '{"count":8,"cursor":"' + end_cursor['cursor'] + '","scale":1.5,"search":null,"id":"' + str(
                encoded_bytes.decode("utf-8")) + '","__relay_internal__pv__VideoPlayerRelayReplaceDashManifestWithPlaylistrelayprovider":false}'
            payload = f'&variables=' + urllib.parse.quote_plus(variables) + '&doc_id=8004529446241809'

            response = requests.request("POST", self.url, headers=self.headers, data=payload, timeout=15)
            return response

        def regex_friend(data_response):

            dataJson = json.loads(data_response.text)

            if not dataJson:
                return
            else:
                edges = dataJson['data']['node']['pageItems']['edges']
                for item in edges:

                    friends_uid = base64.b64decode(str(item['node']['id']).encode("utf-8"))

                    if "" in str(item['node']['url']):
                        friend_url = f'https://www.facebook.com/{str(friends_uid.decode("utf-8")).split("::")[1]}'
                    else:
                        friend_url = item['node']['url']

                    node_fr = {

                        'friends_uid': str(friends_uid.decode("utf-8")).split("::")[1],
                        'friends_name': item['node']['title']['text'],
                        'friends_subtitle': item['node']['subtitle_text']['text'],
                        'frienda_url': friend_url,
                        'friends_id': item['node']['id']
                    }

                    node_friend.append(node_fr)

            return node_friend

        def page_info(data_response):

            dataJson = json.loads(data_response.text)

            has_next_page = dataJson['data']['node']['pageItems']['page_info']['has_next_page']
            cursor = dataJson['data']['node']['pageItems']['page_info']['end_cursor']
            if has_next_page != True:
                return True
            else:
                end_cursor.update({'cursor': cursor})

        while True:
            try:
                data_response = post_request()
                regex_friend(data_response)
                if page_info(data_response):
                    break
                time.sleep(random.randint(1,5))
                print(f'🆗 Đang quét với UID: {uid_profile} {end_cursor}')
            except Exception as e:
                print(f'⚠️ Error: {e}')
        return node_friend


    def start_program(self):
        classname = 'FriendsProfile'
        arraydb = ['100010688473637', '100073488045061']

        for uid_profile in arraydb:
            print(f'✅ Bắt đầu với UID: {uid_profile}')
            node_friend = self.query_friend(uid_profile)
            pagesDB = {'person_uid': uid_profile,
                        'data': node_friend}

            connectMongoDB.insert(pagesDB, classname)
            print(pagesDB, '\n')

if __name__ == '__main__':
	main_app = FriendsProfile()
	main_app.start_program()